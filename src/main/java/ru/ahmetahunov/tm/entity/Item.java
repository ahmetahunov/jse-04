package ru.ahmetahunov.tm.entity;

import java.util.Date;

public interface Item {
    String getId();
    String getName();
    String getDescription();
    Date getStartDate();
    Date getFinishDate();
}
