package ru.ahmetahunov.tm.enumerated;

public enum Operation {
    HELP,
    EXIT,
    PROJECT_CLEAR,
    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_REMOVE,
    PROJECT_SELECT,
    PROJECT_DESCRIPTION,
    TASK_CLEAR,
    TASK_CREATE,
    TASK_LIST,
    TASK_REMOVE,
    TASK_DESCRIPTION,
    TASK_MOVE,
    UNKNOWN
}