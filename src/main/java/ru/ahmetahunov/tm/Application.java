package ru.ahmetahunov.tm;

import ru.ahmetahunov.tm.context.Bootstrap;
import java.io.IOException;

public class Application {

    public static void main( String[] args ) throws IOException {
        Bootstrap bootstrap = new Bootstrap();
        System.out.println( "*** WELCOME TO TASK MANAGER ***" );
        bootstrap.init();
    }

}
