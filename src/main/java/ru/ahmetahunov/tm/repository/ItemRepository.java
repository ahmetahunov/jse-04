package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.entity.Item;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.exception.ItemCollisionException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ItemRepository<T extends Item> {

    Map<String, T> collection = new HashMap<>();

    public void persist(T item) throws IdCollisionException, ItemCollisionException {
        if (collection.containsKey(item.getId()))
            throw new IdCollisionException();
        if (collection.containsValue(item) || "".equals(item.getName()))
            throw new ItemCollisionException();
        collection.put(item.getId(), item);
    }

    public void merge(T item) {
        collection.put(item.getId(), item);
    }

    public void removeAll() {
        collection.clear();
    }

    public T remove(String id){
        return collection.remove(id);
    }

    public Collection<T> findAll() {
        return collection.values();
    }

    public T findOne(String id) {
        return collection.get(id);
    }

}
