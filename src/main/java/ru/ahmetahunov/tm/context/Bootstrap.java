package ru.ahmetahunov.tm.context;

import ru.ahmetahunov.tm.command.CommandExecutor;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.ItemRepository;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class Bootstrap {

    private CommandListener commandListener;

    private CommandExecutor commandExecutor;

    private ProjectService projectService;

    private TaskService taskService;

    private ItemRepository<Project> projectRepository;

    private ItemRepository<Task> taskRepository;

    public void init() throws IOException {
        this.projectRepository = new ItemRepository<>();
        this.taskRepository = new ItemRepository<>();
        this.projectService = new ProjectService(projectRepository);
        this.taskService = new TaskService(taskRepository);
        this.commandExecutor = new CommandExecutor(projectService, taskService);
        this.commandListener = new CommandListener(commandExecutor);
        startCycle();
    }

    private void startCycle() throws IOException {
        commandListener.listen();
    }

}
