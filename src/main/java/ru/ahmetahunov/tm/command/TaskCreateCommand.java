package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

class TaskCreateCommand implements Command {

    private final ProjectService projectService;

    private final TaskService taskService;

    public TaskCreateCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[TASK CREATE]");
        System.out.print("Enter project name or press enter to skip: ");
        Project project = getProject();
        Task task = createNewTask(project);
        try {
            taskService.createNewTask(task);
        } catch (ItemCollisionException e) {
            replaceTask(task);
            return;
        }
        System.out.println("[OK]");
    }

    private Task createNewTask(Project project) throws IOException {
        Task task = new Task();
        System.out.print("Please enter task name: ");
        task.setName(ConsoleHelper.readMessage().trim());
        System.out.print("Please enter description: ");
        task.setDescription(ConsoleHelper.readMessage());
        System.out.print("Please enter start date(example: 01.01.2020): ");
        task.setStartDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        task.setFinishDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        if (project != null) {
            task.setProjectId(project.getId());
        }
        return task;
    }

    private Project getProject() throws IOException {
        String name = ConsoleHelper.readMessage().trim();
        try {
            Project project = projectService.findProject(name);
            return project;
        } catch (NotExistsException e) {
            System.out.println(name + " is not available.");
            System.out.println("Do you want use another project?<y/n>");
            if ("y".equals(ConsoleHelper.readMessage()))
                return getProject();
            return null;
        }
    }

    private void replaceTask(Task task) throws IOException {
        System.out.println("Task already exists.");
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                return;
            }
        }while (!("y".equals(answer)));
        try {
            taskService.removeTask(task.getName(), task.getProjectId());
        } catch (NotExistsException ignore) {
        }
        taskService.updateTask(task);
    }

    @Override
    public String toString() {
        return "task-create: Create new task.";
    }

}