package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;

class ProjectListCommand implements Command {

    private final ProjectService projectService;

    public ProjectListCommand(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void execute() {
        int i = 1;
        System.out.println("[PROJECT LIST]");
        for (Project project : projectService.listAllProjects()) {
            System.out.println(String.format("%d. %s", i++, project.getName()));
        }
    }

    @Override
    public String toString() {
        return "project-list: Show all available projects.";
    }

}