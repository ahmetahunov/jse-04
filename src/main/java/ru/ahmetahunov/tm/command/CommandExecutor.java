package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.enumerated.Operation;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class CommandExecutor {

    private final Map<Operation, Command> commands;

    private final ProjectService projectService;

    private final TaskService taskService;

    public CommandExecutor(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
        commands = new TreeMap<>();
        commands.put(Operation.HELP, new HelpCommand(commands));
        commands.put(Operation.PROJECT_CLEAR, new ProjectClearCommand(projectService, taskService));
        commands.put(Operation.PROJECT_CREATE, new ProjectCreateCommand(projectService, taskService));
        commands.put(Operation.PROJECT_LIST, new ProjectListCommand(projectService));
        commands.put(Operation.PROJECT_REMOVE, new ProjectRemoveCommand(projectService, taskService));
        commands.put(Operation.PROJECT_SELECT, new ProjectSelectCommand(projectService, taskService));
        commands.put(Operation.PROJECT_DESCRIPTION, new ProjectDescriptionCommand(projectService));
        commands.put(Operation.TASK_CLEAR, new TaskClearCommand(taskService));
        commands.put(Operation.TASK_CREATE, new TaskCreateCommand(projectService, taskService));
        commands.put(Operation.TASK_REMOVE, new TaskRemoveCommand(projectService, taskService));
        commands.put(Operation.TASK_LIST, new TaskListCommand(taskService));
        commands.put(Operation.TASK_DESCRIPTION, new TaskDescriptionCommand(projectService, taskService));
        commands.put(Operation.TASK_MOVE, new TaskMoveCommand(projectService, taskService));
        commands.put(Operation.EXIT, new ExitCommand());
    }

    public void execute(Operation operation) throws IOException {
        if (operation == Operation.UNKNOWN) {
            System.out.println("Unknown operation." +
                    " Please enter correct command or 'help' to list available operations.");
            return;
        }
        Command command = commands.get(operation);
        command.execute();
    }

}