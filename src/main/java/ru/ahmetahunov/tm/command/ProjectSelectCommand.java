package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class ProjectSelectCommand implements Command {

    private ProjectService projectService;

    private TaskService taskService;

    public ProjectSelectCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT SELECT]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        try {
            Project project = projectService.findProject(name);
            System.out.println(project.getName());
            int i = 1;
            for (Task task : taskService.listAllProjectTasks(project.getId())) {
                System.out.println(String.format("  %d. %s", i++, task.getName()));
            }
        } catch (NotExistsException e) {
            System.out.println("Selected project not exists.");
        }
    }

    @Override
    public String toString() {
        return "project-select: Show selected project with tasks.";
    }

}
