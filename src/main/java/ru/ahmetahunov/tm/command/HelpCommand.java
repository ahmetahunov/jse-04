package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.enumerated.Operation;
import java.util.Map;

class HelpCommand implements Command {

    Map<Operation, Command> commands;

    public HelpCommand(Map<Operation, Command> commands) {
        this.commands = commands;
    }

    public void execute() {
        for (Command command : commands.values()) {
            System.out.println(command.toString());
        }
    }

    public String toString() {
        return "help: Show all commands.";
    }

}