package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

class TaskRemoveCommand implements Command {

    private ProjectService projectService;

    private TaskService taskService;

    public TaskRemoveCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() throws IOException {
        try {
            System.out.println("[TASK REMOVE]");
            System.out.print("Enter project name or press enter to skip:");
            String projectId = projectService.findProjectId(ConsoleHelper.readMessage().trim());
            System.out.print("Enter task name: ");
            String name = ConsoleHelper.readMessage().trim();
            taskService.removeTask(name, projectId);
        } catch (NotExistsException e) {
            System.out.println("Selected task or project not exists.");
        }
    }

    @Override
    public String toString() {
        return "task-remove: Remove selected task.";
    }

}