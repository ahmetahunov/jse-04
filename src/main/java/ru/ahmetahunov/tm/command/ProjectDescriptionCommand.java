package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public class ProjectDescriptionCommand implements Command {

    private final ProjectService projectService;

    public ProjectDescriptionCommand(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT-DESCRIPTION]");
        System.out.print("Please enter project name: ");
        try {
            Project project = projectService.findProject(ConsoleHelper.readMessage());
            System.out.println(InfoUtil.getInfo(project));
        } catch (NotExistsException e) {
            System.out.println("Selected project not exists.");
        }
    }

    @Override
    public String toString() {
        return "project-description: Show project information.";
    }

}
