package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.service.TaskService;

class TaskClearCommand implements Command {

    private final TaskService taskService;

    public TaskClearCommand(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void execute() {
        taskService.clearRepository();
        System.out.println("[ALL TASKS REMOVED]");
    }

    @Override
    public String toString() {
        return "task-clear: Remove all tasks.";
    }

}