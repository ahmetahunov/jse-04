package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.service.TaskService;

class TaskListCommand implements Command {

    private final TaskService taskService;

    public TaskListCommand(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void execute() {
        int i = 1;
        System.out.println("[TASK LIST]");
        for (Task task : taskService.listAllTasks()) {
            System.out.println(String.format("%d. %s", i++, task.getName()));
        }
    }

    @Override
    public String toString() {
        return "task-list: Show all available tasks.";
    }

}