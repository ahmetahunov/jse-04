package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public class TaskDescriptionCommand implements Command {

    private final ProjectService projectService;

    private final TaskService taskService;

    public TaskDescriptionCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() throws IOException {
        try {
            System.out.println("[TASK-DESCRIPTION]");
            System.out.print("Please enter project name: ");
            String name = ConsoleHelper.readMessage();
            String projectId = projectService.findProjectId(name);
            System.out.print("Please enter task name: ");
            name = ConsoleHelper.readMessage();
            Task task = taskService.findTask(name, projectId);
            System.out.println(InfoUtil.getInfo(task));
        } catch (NotExistsException e) {
            System.out.println("Selected task or project not exists.");
        }
    }

    @Override
    public String toString() {
        return "task-description: Show task description.";
    }

}
