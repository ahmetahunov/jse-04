package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class TaskMoveCommand implements Command {

    private ProjectService projectService;

    private TaskService taskService;

    public TaskMoveCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() throws IOException {
        try {
            System.out.println("[TASK-MOVE]");
            System.out.print("Please enter project name: ");
            String projectId = projectService.findProjectId(ConsoleHelper.readMessage());
            System.out.print("Please enter task name: ");
            String taskName = ConsoleHelper.readMessage();
            Task task = taskService.findTask(taskName, projectId);
            System.out.print("Please enter new project name: ");
            projectId = projectService.findProjectId(ConsoleHelper.readMessage());
            task.setProjectId(projectId);
            System.out.println("[OK]");
        } catch (NotExistsException e) {
            System.out.println("Selected task or project not exists.");
        }
    }

    @Override
    public String toString() {
        return "task-move: Change project for task.";
    }

}
