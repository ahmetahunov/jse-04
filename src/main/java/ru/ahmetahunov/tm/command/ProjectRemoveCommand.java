package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

class ProjectRemoveCommand implements Command {

    private final ProjectService projectService;

    private final TaskService taskService;

    public ProjectRemoveCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        try {
            Project removedProject = projectService.removeProject(name);
            taskService.removeAllProjectTasks(removedProject.getId());
            System.out.println("[OK]");
        } catch (NotExistsException e) {
            System.out.println("Selected project not exists.");
        }
    }

    @Override
    public String toString() {
        return "project-remove: Remove selected project.";
    }

}