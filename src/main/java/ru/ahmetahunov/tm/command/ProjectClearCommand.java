package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;

class ProjectClearCommand implements Command {

    private final ProjectService projectService;

    private final TaskService taskService;

    public ProjectClearCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() {
        for (Project project : projectService.listAllProjects()) {
            taskService.removeAllProjectTasks(project.getId());
        }
        projectService.clearRepository();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public String toString() {
        return "project-clear: Remove all projects.";
    }

}