package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

class ProjectCreateCommand implements Command {

    private final ProjectService projectService;

    private final TaskService taskService;

    public ProjectCreateCommand(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT CREATE]");
        Project project = createNewProject();
        try {
            projectService.addNewProject(project);
        } catch (ItemCollisionException e) {
            replaceProject(project);
            return;
        }
        System.out.println("[OK]");
    }

    private Project createNewProject() throws IOException {
        Project project = new Project();
        System.out.print("Please enter project name: ");
        project.setName(ConsoleHelper.readMessage().trim());
        System.out.println("Please enter description:");
        project.setDescription(ConsoleHelper.readMessage());
        System.out.print("Please insert start date(example: 01.01.2020): ");
        project.setStartDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        project.setFinishDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        return project;
    }

    private void replaceProject(Project project) throws IOException {
        System.out.println("Project with this name already exists.");
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                return;
            }
        } while (!("y".equals(answer)));
        try {
            Project removedProject = projectService.removeProject(project.getName());
            taskService.removeAllProjectTasks(removedProject.getId());
        } catch (NotExistsException ignore) {
        }
        projectService.updateProject(project);
    }

    @Override
    public String toString() {
        return "project-create: Create new project.";
    }

}