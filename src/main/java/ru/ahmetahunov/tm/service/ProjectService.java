package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.repository.ItemRepository;
import java.util.Collection;

public class ProjectService {

    private final ItemRepository<Project> repository;

    public ProjectService(ItemRepository<Project> projectItemRepository) {
        this.repository = projectItemRepository;
    }

    public void addNewProject(Project project) throws ItemCollisionException {
        if (project == null)
            return;
        try {
            repository.persist(project);
        } catch (IdCollisionException e) {
            Project project2 = new Project();
            project2.setName(project.getName());
            project2.setDescription(project.getDescription());
            project2.setStartDate(project.getStartDate());
            project2.setFinishDate(project.getFinishDate());
            addNewProject(project2);
        }
    }

    public void updateProject(Project project) {
        if (project == null)
            return;
        repository.merge(project);
    }

    public Project findProject(String name) throws NotExistsException {
        for (Project project : repository.findAll()) {
            if (project.getName().equals(name))
                return project;
        }
        throw new NotExistsException();
    }

    public Collection<Project> listAllProjects() {
        return repository.findAll();
    }

    public String findProjectId(String name) throws NotExistsException {
        if ("".equals(name))
            return "";
        Project project = findProject(name);
        return project.getId();
    }

    public Project removeProject(String name) throws NotExistsException {
        Project project = findProject(name);
        return repository.remove(project.getId());
    }

    public void clearRepository() {
        repository.removeAll();
    }

}
