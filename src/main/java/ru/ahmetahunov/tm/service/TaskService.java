package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.exception.NotExistsException;
import ru.ahmetahunov.tm.repository.ItemRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TaskService {

    private final ItemRepository<Task> repository;

    public TaskService(ItemRepository<Task> taskItemRepository) {
        this.repository = taskItemRepository;
    }

    public void createNewTask(Task task) throws ItemCollisionException {
        try {
            repository.persist(task);
        } catch (IdCollisionException e) {
            Task task2 = new Task();
            task2.setName(task.getName());
            task2.setDescription(task.getDescription());
            task2.setStartDate(task.getStartDate());
            task2.setFinishDate(task.getFinishDate());
            task2.setProjectId(task.getProjectId());
            createNewTask(task2);
        }
    }

    public void updateTask(Task task) {
        repository.merge(task);
    }

    public Task findTask(String name, String projectId) throws NotExistsException {
        if (projectId == null)
            projectId = "";
        for (Task task : repository.findAll()) {
            if (task.getName().equals(name) && task.getProjectId().equals(projectId))
                return task;
        }
        throw new NotExistsException();
    }

    public List<Task> listAllProjectTasks(String projectId) {
        List<Task> tasks = new ArrayList<>();
        for (Task task : listAllTasks()) {
            if (task.getProjectId().equals(projectId))
                tasks.add(task);
        }
        return tasks;
    }

    public Collection<Task> listAllTasks() {
        return repository.findAll();
    }

    public void removeTask(String name, String projectId) throws NotExistsException {
        Task task = findTask(name, projectId);
        removeTask(task);
    }

    public void removeTask(Task task) {
        repository.remove(task.getId());
    }

    public void removeAllProjectTasks(String projectId) {
        listAllTasks().removeIf(x->x.getProjectId().equals(projectId));
    }

    public void clearRepository() {
        repository.removeAll();
    }

}
